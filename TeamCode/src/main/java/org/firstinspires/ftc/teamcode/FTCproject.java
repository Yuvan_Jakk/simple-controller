package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

@TeleOp(name="Basic: Iterative OpMode", group="Iterative Opmode")
public class FTCproject extends OpMode {
    private DcMotor right_drive;
    private DcMotor right_drive2;
    private DcMotor left_drive;
    private DcMotor left_drive2;

    @Override
    public void init() {
        right_drive = hardwareMap.get(DcMotor.class, "frontright");
        right_drive2 = hardwareMap.get(DcMotor.class, "backright");

        left_drive = hardwareMap.get(DcMotor.class, "frontleft");
        left_drive2 = hardwareMap.get(DcMotor.class, "backleft");

        left_drive.setDirection(DcMotorSimple.Direction.REVERSE);
        left_drive2.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    @Override
    public void loop() {
        double drive = -gamepad1.left_stick_y;
        double turn = gamepad1.right_stick_x;

        left_drive.setPower(drive + turn);
        left_drive2.setPower(drive + turn);
        right_drive.setPower(drive - turn);
        right_drive2.setPower(drive - turn);
    }
}
